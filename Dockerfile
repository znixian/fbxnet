# Source for https://hub.docker.com/r/znixian/mono-swig-cpp

# Take a specific version so there's no chance our stuff
# will magically break one day.
FROM mono:6.4.0

RUN apt-get update && apt-get install -y "g++-6" swig
RUN apt-get install -y make
