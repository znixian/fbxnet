--- orig
+++ patched
@@ -115,12 +115,13 @@
 		eTypeCount
 	};
 
-    const static int sTypeTextureStartIndex = int(eTextureDiffuse);	//!< The start index of texture type layer elements. 
-    const static int sTypeTextureEndIndex = int(eTypeCount) - 1;	//!< The end index of texture type layer elements.
-    const static int sTypeTextureCount = sTypeTextureEndIndex - sTypeTextureStartIndex + 1;	//!< The count of texture type layer elements.
-    const static int sTypeNonTextureStartIndex = int(eNormal);		//!< The start index of non-texture type layer elements.
-    const static int sTypeNonTextureEndIndex = int(eVisibility);	//!< The end index of non-texture type layer elements.
-    const static int sTypeNonTextureCount = sTypeNonTextureEndIndex - sTypeNonTextureStartIndex + 1;	//!< The count of non-texture type layer elements.
+	// MODIFIED!
+    static const int sTypeTextureStartIndex = int(eTextureDiffuse);	//!< The start index of texture type layer elements. 
+    static const int sTypeTextureEndIndex = eTypeCount - 1;	//!< The end index of texture type layer elements.
+    static const int sTypeTextureCount = sTypeTextureEndIndex - sTypeTextureStartIndex + 1;	//!< The count of texture type layer elements.
+    static const int sTypeNonTextureStartIndex = eNormal;		//!< The start index of non-texture type layer elements.
+    static const int sTypeNonTextureEndIndex = eVisibility;	//!< The end index of non-texture type layer elements.
+    static const int sTypeNonTextureCount = sTypeNonTextureEndIndex - sTypeNonTextureStartIndex + 1;	//!< The count of non-texture type layer elements.
     static const char* const sTextureNames[];						//!< Array of names of texture type layer elements.
     static const char* const sTextureUVNames[];						//!< Array of names of UV layer elements.
     static const char* const sNonTextureNames[];					//!< Array of names of non-texture type layer elements.
@@ -1290,6 +1291,9 @@
 	FbxLayerElementArrayTemplate<int>*  mIndexArray;
 #endif /* !DOXYGEN_SHOULD_SKIP_THIS *****************************************************************************************/
 };
+#ifdef SWIG
+%include "FbxMidLayer.i"
+#endif
 
 #define FBXSDK_LAYER_ELEMENT_CREATE_DECLARE(classDesc) \
     FBXSDK_FRIEND_NEW();        \
