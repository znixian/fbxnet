Download the [FBX SDK version 2019.0](https://www.autodesk.com/developer-network/platform-technologies/fbx-sdk-2019-0) and
install it to this directory after agreeing to (and hopefully reading) the licence.
