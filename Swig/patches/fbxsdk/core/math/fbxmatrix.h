--- orig
+++ patched
@@ -211,10 +211,12 @@
 		operator const double* () const;
 
 		//! Define 4*4 array as a new type.
-		typedef const double(kDouble44)[4][4] ;
+		// MODIFIED!
+		// typedef const double(kDouble44)[4][4] ;
 
 		//! Cast the matrix in a reference to a 4*4 array.
-		inline kDouble44 & Double44() const { return *((kDouble44 *)&mData[0][0]); }
+		// MODIFIED!
+		// inline kDouble44 & Double44() const { return *((kDouble44 *)&mData[0][0]); }
 	//@}
 
 	//! \name Math Operations
