%module FbxNet
%{
#include "fbxsdk.h"

typedef fbxsdk::FbxDeformer::EDeformerType EDeformerType;
%}

#pragma SWIG nowarn=503,516

#define __linux__
#define __x86_64__
#define __GNUC__

// Exclude internal-use undocumented stuff
// #define DOXYGEN_SHOULD_SKIP_THIS
// Unfortunately this breaks some classes which have
// access modifiers inside the defines, resulting in stub.cpp
// referencing those functions.

#define FBXSDK_SHARED

%include "fbxsdk.h"

// We NEED raw array access
%include "carrays.i"
%array_functions(double, doubleArray);
%array_functions(int, intArray);

// Core definitions
%include "fbxsdk/core/arch/fbxarch.h"
%include "fbxsdk/core/arch/fbxtypes.h"
%include "fbxsdk/core/arch/fbxdebug.h"
// %include "fbxsdk/core/arch/fbxalloc.h"
%include "fbxsdk/core/arch/fbxnew.h"
// %include "fbxsdk/core/arch/fbxstdcompliant.h"

// AFAIK we have to declare these early to make inheritence work
%template(FbxDouble2) FbxVectorTemplate2<FbxDouble>;
%template(FbxDouble3) FbxVectorTemplate3<FbxDouble>;
%template(FbxDouble4) FbxVectorTemplate4<FbxDouble>;
%template(FbxDouble4x4) FbxVectorTemplate4<FbxDouble4>;

// Renames
// %rename("Add") operator+;
// %rename("Minus") operator-;
%rename("FbxEq") operator==;
%ignore FbxMarkObject;
%ignore FbxCleanUpConnectionsAtDestructionBoundary;

// There are some odd symbols that (at least some of which) don't
// appear in the binaries!
// On Linux it's fine and things will only crash at runtime (if we use
// them, which we won't) but on Windows it won't compile.
%ignore FbxSelectionToTimeFormat;
%ignore FbxSelectionToTimeMode;
%ignore FbxTimeToSelection;
%ignore FbxIOPropInfo; // This entire type is private iirc
%ignore FbxImporter::SetPassword;
%ignore FbxLayerElementMaterial::LayerElementArrayProxy::LayerElementArrayProxy;
%ignore FbxLayerElementMaterial::LayerElementArrayProxy::SetContainer;
%ignore FbxSubDiv::SetLevelCount;

// Other base includes
%include "fbxsdk/core/base/fbxarray.h"
%include "fbxsdk/core/base/fbxbitset.h"
%include "fbxsdk/core/base/fbxcharptrset.h"
%include "fbxsdk/core/base/fbxcontainerallocators.h"
%include "fbxsdk/core/base/fbxdynamicarray.h"
%include "fbxsdk/core/base/fbxstatus.h"
%include "fbxsdk/core/base/fbxfile.h"

%include "fbxsdk/core/base/fbxhashmap.h"
%include "fbxsdk/core/base/fbxintrusivelist.h"
%include "fbxsdk/core/base/fbxmap.h"
%include "fbxsdk/core/base/fbxmemorypool.h"
%include "fbxsdk/core/base/fbxpair.h"
%include "fbxsdk/core/base/fbxset.h"
%include "fbxsdk/core/base/fbxstring.h"
%include "fbxsdk/core/base/fbxstringlist.h"
%include "fbxsdk/core/base/fbxtime.h"
%include "fbxsdk/core/base/fbxtimecode.h"
%include "fbxsdk/core/base/fbxutils.h"

// Math stuff
%include "fbxsdk/core/math/fbxmath.h"
%include "fbxsdk/core/math/fbxdualquaternion.h"
%include "fbxsdk/core/math/fbxmatrix.h"
%include "fbxsdk/core/math/fbxquaternion.h"
%include "fbxsdk/core/math/fbxvector2.h"
%include "fbxsdk/core/math/fbxvector4.h"

%include "fbxsdk/core/math/fbxaffinematrix.h"

// Main APIs
%include "fbxsdk/core/fbxclassid.h"
%include "fbxsdk/core/fbxconnectionpoint.h"
%include "fbxsdk/core/fbxdatatypes.h"
%include "fbxsdk/core/fbxmanager.h"
%include "fbxsdk/core/fbxobject.h"
%include "fbxsdk/core/fbxperipheral.h"
%include "fbxsdk/core/fbxproperty.h"
%include "fbxsdk/core/fbxpropertydef.h"
%include "fbxsdk/core/fbxpropertyhandle.h"
%include "fbxsdk/core/fbxpropertypage.h"
// %include "fbxsdk/core/fbxpropertytypes.h"
%include "fbxsdk/core/fbxquery.h"
%include "fbxsdk/core/fbxqueryevent.h"
%include "fbxsdk/core/fbxxref.h"

%include "fbxsdk/core/fbxevent.h"

// File IO
%ignore ImportThread;
%ignore ExportThread;
%include "fbxsdk/fileio/fbxexporter.h"
%include "fbxsdk/fileio/fbxexternaldocreflistener.h"
%include "fbxsdk/fileio/fbxfiletokens.h"
%include "fbxsdk/fileio/fbxglobalcamerasettings.h"
%include "fbxsdk/fileio/fbxgloballightsettings.h"
%include "fbxsdk/fileio/fbxgobo.h"
%include "fbxsdk/fileio/fbximporter.h"
%include "fbxsdk/fileio/fbxiobase.h"
%include "fbxsdk/fileio/fbxiopluginregistry.h"
%include "fbxsdk/fileio/fbxiosettings.h"
%include "fbxsdk/fileio/fbxstatisticsfbx.h"
%include "fbxsdk/fileio/fbxstatistics.h"

// Scene management
%include "fbxsdk/scene/fbxaudio.h"
%include "fbxsdk/scene/fbxaudiolayer.h"
%include "fbxsdk/scene/fbxcollection.h"
%include "fbxsdk/scene/fbxcollectionexclusive.h"
%include "fbxsdk/scene/fbxcontainer.h"
%include "fbxsdk/scene/fbxcontainertemplate.h"
%include "fbxsdk/scene/fbxdisplaylayer.h"
%include "fbxsdk/scene/fbxdocument.h"
%include "fbxsdk/scene/fbxdocumentinfo.h"
%include "fbxsdk/scene/fbxenvironment.h"
%include "fbxsdk/scene/fbxgroupname.h"
%include "fbxsdk/scene/fbxlibrary.h"
%include "fbxsdk/scene/fbxmediaclip.h"
%include "fbxsdk/scene/fbxobjectmetadata.h"
%include "fbxsdk/scene/fbxpose.h"
%include "fbxsdk/scene/fbxreference.h"
%include "fbxsdk/scene/fbxscene.h"
%include "fbxsdk/scene/fbxselectionset.h"
%include "fbxsdk/scene/fbxselectionnode.h"
%include "fbxsdk/scene/fbxtakeinfo.h"
%include "fbxsdk/scene/fbxthumbnail.h"
%include "fbxsdk/scene/fbxvideo.h"

// This must be included directly after the FbxLayerElementTemplate definition in fbxlayer.h:
// #ifdef SWIG
// %include "FbxMidLayer.i"
// #endif

%include "fbxsdk/scene/geometry/fbxlayer.h"

// Geometry
%ignore GetSubDeformerType;
%ignore PropertyNotify;
%include "fbxsdk/scene/geometry/fbxblendshape.h"
%include "fbxsdk/scene/geometry/fbxblendshapechannel.h"
%include "fbxsdk/scene/geometry/fbxcache.h"
%include "fbxsdk/scene/geometry/fbxcachedeffect.h"
%include "fbxsdk/scene/geometry/fbxcamera.h"
%include "fbxsdk/scene/geometry/fbxcamerastereo.h"
%include "fbxsdk/scene/geometry/fbxcameraswitcher.h"
%include "fbxsdk/scene/geometry/fbxcluster.h"
%include "fbxsdk/scene/geometry/fbxdeformer.h"
%include "fbxsdk/scene/geometry/fbxgenericnode.h"
%include "fbxsdk/scene/geometry/fbxgeometry.h"
%include "fbxsdk/scene/geometry/fbxgeometrybase.h"
%include "fbxsdk/scene/geometry/fbxgeometryweightedmap.h"
%include "fbxsdk/scene/geometry/fbxlight.h"
%include "fbxsdk/scene/geometry/fbxlimitsutilities.h"
%include "fbxsdk/scene/geometry/fbxline.h"
%include "fbxsdk/scene/geometry/fbxlodgroup.h"
%include "fbxsdk/scene/geometry/fbxmarker.h"
%include "fbxsdk/scene/geometry/fbxmesh.h"
%include "fbxsdk/scene/geometry/fbxnode.h"
%include "fbxsdk/scene/geometry/fbxnodeattribute.h"
%include "fbxsdk/scene/geometry/fbxnull.h"
%include "fbxsdk/scene/geometry/fbxnurbs.h"
%include "fbxsdk/scene/geometry/fbxnurbscurve.h"
%include "fbxsdk/scene/geometry/fbxnurbssurface.h"
%include "fbxsdk/scene/geometry/fbxopticalreference.h"
%include "fbxsdk/scene/geometry/fbxpatch.h"
%include "fbxsdk/scene/geometry/fbxproceduralgeometry.h"
%include "fbxsdk/scene/geometry/fbxshape.h"
%include "fbxsdk/scene/geometry/fbxskeleton.h"
%include "fbxsdk/scene/geometry/fbxskin.h"
%include "fbxsdk/scene/geometry/fbxsubdeformer.h"
%include "fbxsdk/scene/geometry/fbxsubdiv.h"
%include "fbxsdk/scene/geometry/fbxtrimnurbssurface.h"
%include "fbxsdk/scene/geometry/fbxvertexcachedeformer.h"
%include "fbxsdk/scene/geometry/fbxweightedmapping.h"

%include "fbxsdk/scene/geometry/fbxlayercontainer.h"

// Why is this such an issue :(
%extend FbxNode {
        void SetNodeAttributeGeom(FbxGeometryBase* geom) {
                $self->SetNodeAttribute(geom);
        }
}

%extend FbxObject {
        // Equality check, since the C# frontend does not seem to create them
        unsigned long long PtrHashCode() {
                return (unsigned long long) $self;
        }
}

// Conversion between FbxMatrix and FbxAMatrix
%extend FbxAMatrix {
        FbxMatrix ToFbxMatrix() {
                return *$self;
        }
}

// Unsafe casting, unfortunately afaik this is
// the only way to do it.
%extend FbxDeformer {
        FbxSkin* CastToSkin() {
                if($self->GetClassId() != FbxSkin::ClassId)
                        return NULL;

                return (FbxSkin*) $self;
        }
}

// Templates
// TODO rearrange and move further up if possible
%template(FbxPropertyDouble3) FbxPropertyT<FbxDouble3>;
%template(FbxPropertyInt) FbxPropertyT<FbxInt>;
%template(FbxPropertyEulerOrder) FbxPropertyT<FbxEuler::EOrder>;
%template(FbxPropertyDouble) FbxPropertyT<FbxDouble>;
%template(FbxLayerElementArrayTemplateVector2) FbxLayerElementArrayTemplate<FbxVector2>;
%template(FbxLayerElementArrayTemplateVector4) FbxLayerElementArrayTemplate<FbxVector4>;
%template(FbxLayerElementArrayTemplateColour) FbxLayerElementArrayTemplate<FbxColor>;
%template(FbxLayerElementArrayTemplateInt) FbxLayerElementArrayTemplate<int>;

// Define this ourselves
class FbxColor {
public:
        FbxColor();
        FbxColor(double r, double g, double b, double a = 1.0);
        ~FbxColor();
        void Set(double r, double g, double b, double a = 1.0);

        double mRed;
        double mGreen;
        double mBlue;
        double mAlpha;
};
