#!/bin/bash

cd "$(dirname "$0")"

# Clear out the patches
rm -rf patches
mkdir patches

# Make these the relevant paths on your machine
pristine=../clean_include/include/
dirty=../linfbx/include/

for f in $(cd ../FBX/include && find . -type f); do
	a="$pristine/$f"
	b="$dirty/$f"
	dc=$(diff --strip-trailing-cr -u "$a" "$b" -Lorig -Lpatched)
	status=$?
	if [[ $status == 0 ]]; then
		continue
	fi
	echo "Generating patch for $f"
	patchfile="patches/$f"
	mkdir -p $(dirname $patchfile)
	echo "$dc" > $patchfile
done
