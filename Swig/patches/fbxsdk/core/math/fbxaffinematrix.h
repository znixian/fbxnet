--- orig
+++ patched
@@ -299,10 +299,11 @@
 		operator double* ();
 		//! Cast the matrix in a const double pointer.
 		operator const double* () const;
+		// MODIFIED!
 		//! Define 4*4 array as a new type 
-		typedef const double(kDouble44)[4][4] ;
+		// typedef const double(kDouble44)[4][4] ;
 		//! Cast the matrix in a reference to a 4*4 array.
-		inline kDouble44 & Double44() const { return *((kDouble44 *)&mData[0][0]); }
+		// inline kDouble44 & Double44() const { return *((kDouble44 *)&mData[0][0]); }
 	//@}
 
 	/** Find out if the matrix is equal to identity matrix.
